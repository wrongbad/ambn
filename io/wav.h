#pragma once

#include <vector>
#include <string>

namespace io {

struct wav
{   // index like: samples[chan_idx + frame_idx * channels]
    std::vector<float> samples;
    int frame_rate = 0;
    int channels = 0;

    wav() = default;
    wav(std::string path, float scale = 1);

    wav & mono();
};

} // io