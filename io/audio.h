#pragma once

#include <vector>
#include <memory>

#include <RtAudio.h>

#include "io/stream.h"

namespace io {

class audio
{
public:
    using RenderFunc = std::function<int(float const*, float*, int)>;

    audio(int channels = 2, int sample_rate = 44100, int buffer_size = 128)
    :   _channels(channels),
        _sample_rate(sample_rate),
        _buffer_size(buffer_size)
    {
    }

    // void add_stream(io::stream o)
    // {
    //     _streams.emplace_back(std::move(o));
    // }
    void start(RenderFunc func)
    {   
        _func = std::move(func);

        auto names = _driver.getDeviceNames();
        for(auto name : names) {
            std::cout << "option audio device name " << name << std::endl;
        }
        auto ids = _driver.getDeviceIds();
        for(auto id : ids) {
            std::cout << "option audio device id " << id << std::endl;
        }
        std::cout << "default audio device id " << _driver.getDefaultOutputDevice() << std::endl;


        RtAudio::StreamParameters out_params;
        out_params.deviceId = _driver.getDefaultOutputDevice();
        out_params.nChannels = _channels;
        out_params.firstChannel = 0;
        
        
        RtAudio::StreamOptions options;
        options.flags = RTAUDIO_SCHEDULE_REALTIME;
        // options.flags |= RTAUDIO_HOG_DEVICE;
        // options.flags |= RTAUDIO_NONINTERLEAVED;

        unsigned buffer_request = _buffer_size;
        _driver.openStream(
            &out_params, 
            nullptr, // input params
            RTAUDIO_FLOAT32, 
            _sample_rate,
            &buffer_request,
            &render_wrapper,
            (void*)&_func,
            &options
        );
        _driver.startStream();
    }
    void stop()
    {   _driver.stopStream();
        _driver.closeStream();
    }
    int rate() const
    {   return _sample_rate;
    }
    int channels() const
    {   return _channels;
    }

    // int render(float const* in, float * out, int frames)
    // {   
    //     for(int i=0 ; i<frames * _channels ; i++)
    //     { 
    //         out[i] = 0;
    //     }
        
    //     int err = 0;
    //     for(auto & s : _streams)
    //     {   
    //         err |= s->render(in, out, frames);
    //     }

    //     for(int i=0 ; i<frames * _channels ; i++)
    //     {
    //         const float clip = 10;
    //         if(!(out[i] > -clip)) { out[i] = -clip; }
    //         if(!(out[i] < clip)) { out[i] = clip; }
    //     }

    //     return err;
    // }

    static int render_wrapper(void * out, void * in, unsigned frames,
            double /*time*/, RtAudioStreamStatus /*status*/, void * ctx)
    {
        // return ((audio*)ctx)->render((float const*)in, (float *)out, (int)frames);
        RenderFunc & func = *(RenderFunc*)ctx;
        return func((float const*)in, (float *)out, (int)frames);
    }

private:
    int _channels;
    int _sample_rate;
    int _buffer_size;
    RtAudio _driver;
    RenderFunc _func;
    // std::vector<io::stream> _streams;
};

} // pc