#pragma once

#include <memory>

namespace io {

struct stream_base
{
    virtual ~stream_base() {}
    virtual int render(float const* in, float * out, int frames) = 0;
};

using stream = std::shared_ptr<io::stream_base>;

// template<Stream>
// struct cached_stream : stream
// {
//     int render(float const* in, float * out, int frames) override
//     {
//         ((Stream*)this)->read(in, out, frames);
//     }
// }

} // io