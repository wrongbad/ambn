#define DR_WAV_IMPLEMENTATION
#include "third_party/dr_libs/dr_wav.h"

#include "wav.h"

#include <fstream>



namespace io {


template<class Stream>
Stream & operator>>(Stream && in, wav & audiodata)
{
    auto read = +[] (void * ctx, void * buf, size_t count) {
        Stream & in = *(Stream*)ctx;
        in.read((char*)buf, count);
        return (size_t)in.gcount();
    };
    auto seek = +[] (void * ctx, int off, drwav_seek_origin drway) {
        Stream & in = *(Stream*)ctx;
        std::ios_base::seekdir way = (drway == drwav_seek_origin_start) ?
            std::ios_base::beg : std::ios_base::cur;
        in.seekg(off, way);
        return (uint32_t)(bool)in;
    };
    drwav reader;
    if(!drwav_init_ex(&reader, read, seek, nullptr, (void*)&in,
        nullptr, DRWAV_SEQUENTIAL, nullptr))
    {   in.setstate(std::ios::failbit);
        return in;
    }
    audiodata.channels = reader.channels;
    audiodata.frame_rate = reader.sampleRate;
    size_t frames = reader.totalPCMFrameCount;
    audiodata.samples.resize(frames * audiodata.channels);

    frames = drwav_read_pcm_frames_f32(&reader, frames, audiodata.samples.data());
    audiodata.samples.resize(frames * audiodata.channels); // handle truncated files

    drwav_uninit(&reader);

    return in;
}

template<class Stream>
Stream & operator<<(Stream && out, wav const& audiodata)
{
    auto write = +[] (void * ctx, void const* buf, size_t count) {
        Stream & out = *(Stream*)ctx;
        out.write((char*)buf, count);
        return bool(out) ? count : size_t(0); // approx
    };
    drwav_data_format format;
    format.container = drwav_container_riff;
    format.format = DR_WAVE_FORMAT_IEEE_FLOAT;
    format.channels = audiodata.channels;
    format.sampleRate = audiodata.frame_rate;
    format.bitsPerSample = 32;
    drwav writer;
    if(!drwav_init_write_sequential(&writer, &format,
        audiodata.samples.size(), write, (void*)&out, nullptr))
    {   out.setstate(std::ios::failbit);
        return out;
    }
    drwav_write_pcm_frames(&writer,
        audiodata.samples.size()/audiodata.channels,
        audiodata.samples.data());
    drwav_uninit(&writer);
    return out;
}

wav::wav(std::string path, float scale)
{
    std::ifstream(path) >> *this;    
    if(channels == 0) { throw std::runtime_error("audio: "+path); }
    for(float & x : samples) { x *= scale; }
}

wav & wav::mono()
{
    for(int i=0 ; i<samples.size()/channels ; i++)
    {
        samples[i] = samples[i*channels];
    }
    samples.resize(samples.size()/channels);
    channels = 1;
    return *this;
}

} // namespace