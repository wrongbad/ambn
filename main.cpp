#include <iostream>
#include <chrono>
#include <thread>
#include <sstream>

#include "RtMidi.h"
#include "io/audio.h"

#include "blocks/chimes.h"
#include "blocks/rain.h"
#include "blocks/thunder.h"
#include "blocks/fart.h"
#include "blocks/looper.h"

#include "wade/wmath.h"

#include "httplib.h"


std::string audio_dir = PLANT_AUDIO_DIR;
std::string web_dir = PLANT_WEB_DIR;

struct Patchbay
{
    struct Param
    {
        std::string name;
        std::atomic<float> * param = nullptr;
        int base_val = 0;
        int sens_val = 0;
        int midi_ctl = -1;
    };

    std::vector<Param> params;

    static int float2midi(float f)
    {
        return std::max(0, std::min<int>(std::round(f * 127), 127));
    }
    static float midi2float(int m)
    {
        return std::max(0.0f, std::min(m/127.0f, 1.5f));
    }

    void add(std::string name, std::atomic<float> & param)
    {
        params.push_back({name, &param, float2midi(param.load()), 0, -1});
    }

    void midi_recv(double t, std::vector<uint8_t> const& msg)
    {
        int len = msg.size();
        uint8_t const* m = msg.data();
        if(len == 3 && (m[0] & 0xF0) == 0xB0)
        {
            for(Param & p : params)
            {
                if(p.midi_ctl == m[1])
                {
                    p.sens_val = m[2] - 64;
                    p.param->store(midi2float(p.base_val + p.sens_val));
                }
            }
        }
    }

    std::string get_params()
    {
        std::ostringstream oss;
        for(Param & p : params)
        {
            oss << p.name << ":" 
                << p.midi_ctl << ":" 
                << p.base_val << ",";
        }
        return oss.str();
    }

    void set_midi(std::string const& name, int midi)
    {
        for(Param & p : params)
        {
            if(p.name == name) { p.midi_ctl = midi; }
        }
    }
    void set_value(std::string const& name, int val)
    {
        for(Param & p : params)
        {
            if(p.name == name)
            {
                p.base_val = val;
                p.param->store(midi2float(p.base_val + p.sens_val));
            }
        }
    }
};



struct Engine
{
    Patchbay patchbay;
    io::audio audio {1, 44100, 256};

    RtMidiIn midi_in;
    RtMidiOut midi_out[2];

    blocks::chimes<> chimes;
    blocks::rain rain;
    blocks::thunder thunder;

    std::atomic<bool> do_lightning {false};
    std::atomic<bool> did_lightning {false};

    blocks::rate_looper drone {{
        io::wav(audio_dir+"grandma_garden2.wav", 1).mono(), 
        io::wav(audio_dir+"keep_claim_check.wav", 1).mono(),
        io::wav(audio_dir+"highendcrystals_edit.wav", 1).mono(),
        io::wav(audio_dir+"padorgan.wav", 1).mono(),
        io::wav(audio_dir+"reface_cut.wav", 0.2).mono(),
    }};

    blocks::rate_looper melodic {{
        io::wav(audio_dir+"bass_guitar.wav", 1).mono(),
        io::wav(audio_dir+"flutey.wav", 1).mono(),
        io::wav(audio_dir+"crystalbowl.wav", 1).mono(),
        io::wav(audio_dir+"upright_bass.wav", 1).mono(),
        io::wav(audio_dir+"analog_pad.wav", 1).mono(),
    }};

    blocks::rate_looper waves {{
        io::wav(audio_dir+"waves_filtered.wav", 1).mono(),
    }};
    
    blocks::rate_looper crickets {{
        io::wav(audio_dir+"brazil_crickets.wav", 1).mono(),
        io::wav(audio_dir+"africa_crickets.wav", 1).mono(),
    }};

    blocks::rate_looper creaker {{
        io::wav(audio_dir+"creaker.wav", 1).mono(),
    }};

    blocks::rate_looper birds {{
        io::wav(audio_dir+"birds_filtered.wav", 1).mono(), 
        io::wav(audio_dir+"eagle_owl.wav", 1).mono(),
        io::wav(audio_dir+"reverse_birds_filtered.wav", 1).mono(),
        io::wav(audio_dir+"tawny_owl.wav", 1).mono(),
        io::wav(audio_dir+"loon.wav", 0.3).mono(),
        io::wav(audio_dir+"barred_owl.wav", 1).mono(),
    }};


    void midi_recv(double t, std::vector<uint8_t> & msg)
    {
        patchbay.midi_recv(t, msg);
        
        if(midi_out[0].isPortOpen() && msg[1] % 2 == 0)
        {
            midi_out[0].sendMessage(&msg);
        }

        if(midi_out[1].isPortOpen() && msg[1] % 2 == 1)
        {
            midi_out[1].sendMessage(&msg);
        }

        if(do_lightning && !did_lightning)
        {
            msg[1] = 10;
            msg[2] = 1;
            if(midi_out[0].isPortOpen())
                midi_out[0].sendMessage(&msg);
            if(midi_out[1].isPortOpen())
                midi_out[1].sendMessage(&msg);
            did_lightning = true;
        }
    }

    void connect_midi()
    {
        midi_in.setCallback(+[] (double t, std::vector<uint8_t>* msg, void* ctx) {
                if(msg) { ((Engine*)ctx)->midi_recv(t, *msg); }
            }, this );


        for(int i=0 ; i<(int)midi_in.getPortCount() ; i++)
        {
            std::cout << "in " << i << " " << midi_in.getPortName(i) << std::endl;
        }

        for(int i=0 ; i<(int)midi_in.getPortCount() ; i++)
        {
            if(midi_in.getPortName(i).find("Teensy") == 0)
            {
                midi_in.openPort(i);
                break;
            }
        }
        
        for(int i=0 ; i<(int)midi_out[0].getPortCount() ; i++)
        {
            std::cout << "out " << i << " " << midi_out[0].getPortName(i) << std::endl;
        }

        int out0 = -1;
        for(int i=0 ; i<(int)midi_out[0].getPortCount() ; i++)
        {
            if(midi_out[0].getPortName(i).find("Arduino") == 0)
            {
                midi_out[0].openPort(i);
                out0 = i;
                break;
            }
        }

        for(int i=0 ; i<(int)midi_out[1].getPortCount() ; i++)
        {
            if(i != out0 && midi_out[1].getPortName(i).find("Arduino") == 0)
            {
                midi_out[1].openPort(i);
                break;
            }
        }
    }

    void run()
    {
        connect_midi();

        chimes.amp = 0.3;
        
        rain.amp = 0.15;

        thunder.amp = 0.2;
        thunder.rate = 0.1;

        waves.amp = 0.6;
        waves.rate = 0.6;
        
        drone.amp = 0.1;
        melodic.amp = 0.1;
        birds.amp = 0.05;
        crickets.amp = 0.03;
        crickets.rate = 0.8;
        creaker.amp = 0.02;

        // debug

        // thunder.amp = 1;
        // thunder.rate = 1;

        // chimes.amp = 0;
        // waves.amp = 0;
        // rain.amp = 0;
        // drone.amp = 0;
        // melodic.amp = 0;
        // birds.amp = 0;
        // crickets.amp = 0;
        // creaker.amp = 0.02;


        patchbay.add("chimes_volume", chimes.amp);
        patchbay.add("rain_volume", rain.amp);
        patchbay.add("rain_filter", rain.cutoff);
        patchbay.add("thunder_volume", thunder.amp);
        patchbay.add("thunder_rate", thunder.rate);
        patchbay.add("waves_volume", waves.amp);
        patchbay.add("waves_rate", waves.rate);
        patchbay.add("drone_volume", drone.amp);
        patchbay.add("drone_rate", drone.rate);
        patchbay.add("melodic_volume", melodic.amp);
        patchbay.add("melodic_rate", melodic.rate);
        patchbay.add("birds_volume", birds.amp);
        patchbay.add("birds_rate", birds.rate);
        patchbay.add("crickets_volume", crickets.amp);
        patchbay.add("crickets_rate",  crickets.rate);
        patchbay.add("creaks_volume", creaker.amp);

        patchbay.set_midi("thunder_rate", 0);
        patchbay.set_midi("thunder_volume", 0);

        patchbay.set_midi("crickets_rate", 1);
        patchbay.set_midi("crickets_volume", 1);

        patchbay.set_midi("melodic_volume", 2);

        patchbay.set_midi("drone_rate", 3);

        patchbay.set_midi("creaks_volume", 4);
        patchbay.set_midi("creaks_rate", 4);

        // patchbay.set_midi("rain_volume", 5);
        patchbay.set_midi("rain_filter", 5);
        patchbay.set_midi("waves_volume", 5);
        patchbay.set_midi("waves_rate", 5);

        patchbay.set_midi("birds_volume", 6);

        audio.start([this] (float const* in, float * out, int frames) {
            for(int i=0 ; i<frames ; i++) { out[i] = 0; }
            
            int err = 0;
            err |= chimes.render(in, out, frames);
            err |= rain.render(in, out, frames);
            err |= thunder.render(in, out, frames);
            err |= waves.render(in, out, frames);
            err |= drone.render(in, out, frames);
            err |= melodic.render(in, out, frames);
            err |= birds.render(in, out, frames);
            err |= crickets.render(in, out, frames);
            err |= creaker.render(in, out, frames);

            for(int i=0 ; i<frames ; i++)
            {
                const float clip = 0.9;
                if(!(out[i] > -clip)) { out[i] = -clip; }
                if(!(out[i] < clip)) { out[i] = clip; }
            }

            do_lightning = (thunder.stim > 0.6);
            if(thunder.stim < 0.1) did_lightning = false;

            return err;
        });

        run_server(); // blocking doesn't return
    }

    void run_server()
    {
        httplib::Server svr;

        svr.set_mount_point("/", web_dir);

        svr.Get("/params", [this] (httplib::Request const& req, httplib::Response & res) {
            res.set_content(patchbay.get_params(), "text/plain");
        });

        svr.Post("/params/([\\w\\.]+)", [this] (httplib::Request const& req, httplib::Response & res) {
            std::string param_id = req.matches[1];
            std::string midi_set = req.get_param_value("midi");
            std::string val_set = req.get_param_value("val");
            if(midi_set.size())
            {
                patchbay.set_midi(param_id, std::stoi(midi_set));
            }
            if(val_set.size())
            {
                patchbay.set_value(param_id, std::stoi(val_set));
            }
            res.set_content("", "text/plain");
        });
        
        svr.listen("0.0.0.0", 80); // blocks
    }
};

int main()
{
    Engine e;
    e.run();
    return 0;
}