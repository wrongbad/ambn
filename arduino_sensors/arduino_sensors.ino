#include <Bounce.h>

void setup()
{
    Serial.begin(115200);
}

elapsedMillis msec = 0;

float ftouch1[7] = {0};
float ftouch2[7] = {0};
int midisent[7] = {0};

void loop()
{
    if (msec >= 20)
    {
        msec -= 20;
        
        int touch[7] = {
            touchRead(A9),
            touchRead(A8),
            touchRead(A5),
            touchRead(A4),
            touchRead(A3),
            touchRead(A2),
            touchRead(A1),
        };
        
        int midival[7];
        for(int i=0 ; i<7 ; i++)
        {
            float f = logf(touch[i]);
            ftouch1[i] += (f - ftouch1[i]) * 0.05f;
            ftouch2[i] += (ftouch1[i] - ftouch2[i]) * 0.003f;
            
            int v = (ftouch1[i] - ftouch2[i]) * 1000 + 64;
            if(v < 0) v = 0;
            if(v > 127) v = 127;
            midival[i] = v;
        }
        
        //    Serial.printf("%5d %5d %5d %5d %5d %5d %5d\n",
        //        touch[0], touch[1], touch[2], touch[3], 
        //        touch[4], touch[5], touch[6] );
        
        //    Serial.printf("%2.3f %2.3f %2.3f %2.3f %2.3f %2.3f %2.3f\n",
        //        fsend[0], fsend[1], fsend[2], fsend[3], 
        //        fsend[4], fsend[5], fsend[6] );
        
        
        Serial.printf("%3d %3d %3d %3d %3d %3d %3d\n",
            midival[0], midival[1], midival[2], midival[3], 
            midival[4], midival[5], midival[6] );
        
        for(int i=0 ; i<7 ; i++)
        {
            constexpr int midi_chan = 1;
            if(midival[i] != midisent[i])
                usbMIDI.sendControlChange(i, midival[i], midi_chan);
            midisent[i] = midival[i];
        }
    }
    
    // clear incoming msg queue
    while (usbMIDI.read()) { }
}
