sudo apt install -y git
sudo apt install -y libasound2-dev python3-pip
pip install cmake

sudo systemctl reboot

git clone https://gitlab.com/wrongbad/ambn.git /plant
git submodule update --init


cd /plant
cmake -S . -B build
cmake --build build -j

sudo nano /etc/rc.local
```
/plant/build/audio_engine >/tmp/plant_log.txt 2>&1 &
```



### https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-routed-wireless-access-point

sudo apt install hostapd
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo apt install dnsmasq
sudo DEBIAN_FRONTEND=noninteractive apt install -y netfilter-persistent iptables-persistent


sudo nano /etc/dhcpcd.conf
```
interface wlan0
    static ip_address=192.168.4.1/24
    nohook wpa_supplicant
```


sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
sudo nano /etc/dnsmasq.conf
```
# Listening interface
interface=wlan0
# Pool of IP addresses served via DHCP
dhcp-range=192.168.4.20,192.168.4.40,255.255.255.0,24h
# Local wireless DNS domain
domain=wlan
# Alias for this router
address=/plant.local/192.168.4.1
```



sudo rfkill unblock wlan

sudo nano /etc/hostapd/hostapd.conf
```
country_code=US
interface=wlan0
ssid=plantronics
hw_mode=g
channel=7
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=blueberry
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```


sudo systemctl reboot
