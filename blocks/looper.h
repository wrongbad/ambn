#pragma once

#include <fstream>
#include <stdexcept>
#include <atomic>

#include "io/wav.h"
#include "wade/filter.h"

namespace blocks {




struct looper
{
    io::wav wav;
    int head = 0;

    looper(io::wav audio)
    :   wav(std::move(audio))
    {
    }

    float next()
    {
        float x = wav.samples[head];
        head = (head + 1) % wav.samples.size();
        return x;
    }
};


struct rate_looper : io::stream_base
{
    std::vector<looper> loops;

    float sample = 0;
    float t = 0;

    wade::va_cheby1<7, 3> filter;

    std::atomic<float> amp = 0.1;
    float samp = 0;

    std::atomic<float> rate = 1;
    float srate = rate;

    double phase = 0;
    double dphase = 1e-7;

    rate_looper(std::vector<io::wav> paths)
    {
        for(auto & p : paths) { loops.emplace_back(std::move(p)); }
    }

    rate_looper(io::wav path)
    : rate_looper(std::vector<io::wav>{path})
    {
    }

    float next()
    {
        float x = 0;
        float n = loops.size();
        for(int i=0 ; i<n ; i++)
        {
            float p = fast::sin2pi(phase + i / n);
            p = 0.5 + 0.5 * p;
            if(n > 2) { p = p * p;}
            x += loops[i].next() * p;
        }
        return x;
    }

    int render(float const* in, float * out, int frames)
    {
        for(int i=0 ; i<frames ; i++)
        {
            phase += dphase - (phase > 1);
            srate += ((0.5 + 0.5 * rate) - srate) * 3e-5;
            samp += (amp - samp) * 3e-5;

            float fs = std::min(1.0f, 1/srate);
            float t2 = t + srate;

            while(t2 >= 1)
            {
                filter.step(sample, 1 * fs);
                sample = next();
                t2 -= 1;
            }

            t = t2;
            out[i] += samp * filter.project(sample, t * fs);
        }
        return 0;
    }
};






} // blocks