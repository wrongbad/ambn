#pragma once

#include <atomic>
#include <cmath>

#include "wade/wmath.h"
#include "wade/filter.h"
#include "wade/reverb.h"

namespace blocks {

struct thunder : io::stream_base
{
    int render(float const* in, float * out, int frames) override
    {   
        pressure += (rng() % 100) * frames * 3e-7;
        if(pressure > (1 / (0.01 + rate)))
        {
            stim = (rng() % 1000) / 1000.0f;
            pressure = 0;
        }
        for(int i=0 ; i<frames ; i++)
        {
            stim *= decay;
            samp += (amp - samp) * 3e-5;
            
            float r0 = (rng() % 3) / 2.0f - 0.5f;
            float rum = filter0(r0 * stim).lp();
            rum *= 1000;

            // if(i == 0) { std::cout << rum << std::endl; }

            if(rum < 0.03) { rum = 0; }
            // if(rum > 1) { rum = 1; }

            // n = std::tanh(n / 1000) * 200;

            // float n = (rng() % 5) / 4.0f - 0.5f;

            // float open = stim * rum;
            // if(open > 1) { open = 1; }
            // open = open * open * open;
            // float open = 0.01 * stim;

            filter1.set(0.006 + 0.01 * stim, 0.2 + 0.1 * stim);

            // filter1.set(0.006, 0.2);
            float n = filter1(rum).lp();
            
            n = dcblock(n);

            n = std::tanh(n*10)/10;

            n = reverb(n);

            // n = std::max(-1.0f, std::min(n, 1.0f));
            // n = n * 10;
            
            out[i] += n * amp;
        }
        return 0;
    }

    std::atomic<float> amp = 1;
    float samp = 1;
    float freq = 0.01;
    float decay = 0.99996;
    float stim = 0;
    float pressure = 0;
    std::atomic<float> rate = 0.2;
    wade::rng64 rng;
    wade::svf filter0 {0.001, 0.8};
    wade::svf filter1 {0.01, 0.2};
    wade::filter<2, 2> dcblock {{1, -1}, {1, -0.995}};
    // wade::filter<2, 2> dcblock {{1, -1}, {1, -0.995}};
    wade::reverb2 reverb {0.9};
};

} // blocks