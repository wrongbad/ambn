#pragma once

#include <atomic>
#include <cmath>

#include "wade/wmath.h"
#include "wade/filter.h"

namespace blocks {

struct rain : io::stream_base
{
    int render(float const* in, float * out, int frames) override
    {   
        for(int i=0 ; i<frames ; i++)
        {
            samp += (amp - samp) * 3e-5;
            scutoff += (cutoff - scutoff) * 1e-5;

            float n = (rng() % 3) / 2.0f - 0.5f;

            n = filter0(n).lp(); // slow roll LP

            filter1.set(0.08 + 0.03 * lfo0(), 0.99);
            n = filter1(n).hp(); // high pass modulation

            float m1 = (lfo1()+1) * 0.5;
            filter2.set(scutoff + 0.01 + 0.1 * m1, 0.79 - 0.2 * m1);
            n = filter2(n).lp();
            n /= (0.66 - 0.2 * m1);

            out[i] += n * samp * 0.5f;
        }
        return 0;
    }

    float freq = 0.01;
    std::atomic<float> amp = 0.1;
    double samp = 0;
    std::atomic<float> cutoff = 0.5;
    double scutoff = 0.5;
    wade::rng64 rng;
    wade::svf filter0 {0.5, 2};
    wade::svf filter1 {0.2, 0.3};
    wade::svf filter2 {0.2, 0.3};
    wade::sinwave lfo0 {1e-6};
    wade::sinwave lfo1 {4e-7};
};

} // blocks