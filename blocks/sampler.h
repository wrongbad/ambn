#pragma once

#include <fstream>

#include "io/wav.h"

namespace blocks {

struct sampler
{
    io::wav sample[10];

    sampler(std::string dir = "~/projects/audio/cp73/glocken/")
    {
        std::ifstream(dir+"n48_v127.wav") >> sample[0];
        std::ifstream(dir+"n50_v127.wav") >> sample[1];
        std::ifstream(dir+"n52_v127.wav") >> sample[2];
        std::ifstream(dir+"n55_v127.wav") >> sample[3];
        std::ifstream(dir+"n57_v127.wav") >> sample[4];
    }

    int render(float const* in, float * out, int frames)
    {
        
    }

};



} // blocks