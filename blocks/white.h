#pragma once

#include <cmath>
#include "wade/wmath.h"
#include "wade/filter.h"

namespace blocks {

struct white : io::stream_base
{
    int render(float const* in, float * out, int frames) override
    {   
        for(int i=0 ; i<frames ; i++)
        {
            // float n = (rng() & 0xF) / 16.0f - 0.5f;
            float n = (rng() % 4) / 4.0f - 0.5f;

            filter1.set(0.08 + 0.03 * lfo0(), 1);

            n = filter0(n).lp();
            n = filter1(n).hp();

            float m1 = lfo1();
            filter2.set(0.55 - 0.5 * m1, 0.7 - 0.4 * m1);
            n = filter2(n).lp();
            n /= (0.66 - 0.6 * m1);

            out[i] += n * amp;

            // amp *= decay;
        }
        return 0;
    }

    float freq = 0.01;
    float decay = 0.9996;
    float amp = 0;
    wade::rng64 rng;
    wade::svf filter0 {0.5, 2};
    wade::svf filter1 {1, 0.2};
    wade::svf filter2 {1, 0.2};
    wade::sinwave lfo0 {1e-6};
    wade::sinwave lfo1 {1e-6};
};

} // blocks