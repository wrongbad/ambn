#pragma once

#include <cmath>
#include "wade/wmath.h"
#include "wade/filter.h"
#include "wade/reverb.h"

namespace blocks {

struct fart : io::stream_base
{
    int render(float const* in, float * out, int frames) override
    {   
        for(int i=0 ; i<frames ; i++)
        {
            stim *= decay;
            squeeze += (stim - squeeze) * 0.001;

            if(squeeze > 3e-2)
                pressure += squeeze / 200;
            
            if(pressure > close[mode]) { mode = 1; }
            else { mode = 0; }

            float x = 0;
            if(mode == 1)
            {
                x = pressure * egress;
                pressure -= x;
            }

            x = filter0(x).lp() * 100;

            x = dcblock(x);

            // x = reverb(x);

            out[i] += x;

        }
        return 0;
    }

    float squeeze = 0;
    float pressure = 0;
    int mode = 0;
    float close[2] = {2, 0.5};
    float egress = 0.02;
    float decay = 0.9999;
    float stim = 0;
    wade::rng64 rng;
    wade::svf filter0 {0.01, 0.3};
    // wade::svf filter1 {0.01, 0.2};
    wade::filter<2, 2> dcblock {{1, -1}, {1, -0.99}};
    wade::reverb reverb {0};
};

} // blocks