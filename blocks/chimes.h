#pragma once

#include <atomic>
#include <cstdlib>

#include "wade/wmath.h"
#include "dumb_sounds.h"

namespace blocks {

struct chime : io::stream_base
{
    static constexpr int N = 4;

    int render(float const* in, float * out, int frames) override
    {   
            // for(int h=1 ; h<N ; h++)
            // {
            //     f[h] = h * freq * std::sqrt(1 + inharm * h * h);
            //     if(f[h] > 0.45) { continue; }
            //     amp[h] += stim * fast::exp(h * stim_rolloff) * att;
            // }
        for(int i=0 ; i<frames ; i++)
        {
            float att = 0.003;
            for(int h=1 ; h<N ; h++)
            {
                f[h] = h * freq * std::sqrt(1 + inharm * h * h);
                if(f[h] > 0.45) { continue; }
                amp[h] += stim * fast::exp(h * stim_rolloff) * att;

                amp[h] *= 1 + decay + ((h-1) * 0.6 * decay);
                phase[h] += f[h] - (phase[h] > 1);
                out[i] += fast::sin2pi(phase[h]) * amp[h];
            }

            stim *= (1-att);
        }
        return 0;
    }

    float freq = 0.01;
    float stim = 0;
    float inharm = 0.008;
    float stim_rolloff = -1.2;
    // float decay = 0.9996;
    double decay = -0.00006;
    double amp[N] = {0};
    float f[N] = {0};
    double phase[N] = {0};
};


template<class Sound = chime>
struct chimes : io::stream_base
{
    chimes(int poly = 5, float fmin = 440.0f/44100)
    {
        auto n2f = [] (int n) { return std::pow(2.0f, n/12.0f); };
        // float scale[] = { n2f(0), n2f(2), n2f(4), n2f(7), n2f(9), 
        //         2*n2f(0), 2*n2f(2), 2*n2f(4), 2*n2f(7), 2*n2f(9) };
        float scale[] = { n2f(0), n2f(2), n2f(3), n2f(5), n2f(7), 
                n2f(8), n2f(10), n2f(12) };
        for(int i=0 ; i<poly ; i++)
        {
            _sounds.emplace_back();
            _sounds.back().freq = fmin * scale[i % 10];
        }
        
        // _sounds[0].freq /= 2;
        // _sounds[0].inharm = 0;
        // _sounds[0].decay = -0.00001;
        // _sounds[0].stim_rolloff = -2;
    }

    int render(float const* in, float * out, int frames) override
    {
        float rand = (_rng() % int(1e6)) / float(1e6);
        if(rand < sparsity * frames)
        {
            int voice = _rng() % _sounds.size();
            float stim = (_rng() % 1000) / 1000.0f;
            // if(voice == 0) { stim *= 16; }
            _sounds[voice].stim = 0.1 * stim * amp;
        }

        int err = 0;
        for(auto & s : _sounds)
        {
            err |= s.render(in, out, frames);
        }
        return err;
    }

    float sparsity = 1e-5;
    std::atomic<float> amp = 1;
    std::vector<Sound> _sounds;
    wade::rng64 _rng;
}; 

} // blocks