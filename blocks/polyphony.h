#pragma once

#include "arr.h"
#include "synth.h"

namespace wade {


template<class T, int N>
struct fifo
{
    T _data[N];
    int _read;
    int _write;
    int _size;
    
    void push(T x)
    {   _data[_write] = x;
        _write = (_write + 1) % N;
        _size += 1;
    }
    T pop()
    {   T x = _data[_read];
        _read = (_read + 1) % N;
        _size -= 1;
        return x;
    }
    int size() const { return _size; }
};


template<class MonoSynth, int MaxPoly = 16, int BufSize = 128>
struct poly
{   
    static constexpr int num_controls() { return MonoSynth::num_controls(); }
    
    static constexpr int stack_size() { return 32; }

    struct Note
    {   int8_t note;
        uint8_t vel;
        int voice; 
    };
    struct Voice
    {   MonoSynth synth;
        int note;
    };

    wade::arr<MonoSynth, MaxPoly> _voices;

    wade::fifo<int, MaxPoly> _off;
    // wade::fifo<int, MaxPoly> _on;

    wade::arr<Note, stack_size()> _notes;
    int _n_notes = 0;

    const int _poly = MaxPoly;

    wade::arr<float, BufSize> _audio_tmp;

    poly(MonoSynth synth)
    :   _voices(synth)
    {
        for(int i=0 ; i<MaxPoly ; i++)
        {   _off.push(i);
        }
    }

    void note_on(uint8_t note, uint8_t vel)
    {   
        int vi = -1;
        if(_n_notes < _poly) { vi = _off.pop(); }
        else
        { 
            vi = _notes[_n_notes-_poly].voice;
            _voices[vi].note_off(note, 0);
        }

        _notes[_n_notes] = Note{int8_t(note), vel, vi};
        _n_notes += 1;

        _voices[vi].note_on(note, vel);    
    }
    
    void note_off(uint8_t note, uint8_t vel)
    {   
        // find note
        int ni = -1;
        for(int i=_n_notes-1 ; i>=0 ; i--)
        {   if(_notes[i].note == note) { ni = i; }
        }
        if(ni < 0) { return; }
        // turn off if active
        if(ni + _poly >= _n_notes)
        {
            int vi = _notes[ni].voice;
            _voices[vi].note_off(note, vel);
            _off.push(vi);

        }
        // delete and shift back
        _n_notes -= 1;
        for(int i=ni ; i<_n_notes ; i++)
        {   _notes[i] = _notes[i+1];
        }
        // resume overbooked notes
        if(_n_notes >= _poly)
        {
            int ni = _n_notes-_poly;
            int vi = _off.pop();
            _notes[ni].voice = vi;
            _voices[vi].note_on(_notes[ni].note, _notes[ni].vel);
        }
    }

    void note_pressure(uint8_t note, uint8_t press)
    {   

    }

    void render(float * buf, int len)
    {   wade_assert(len <= BufSize);
        for(int i=0 ; i<len ; i++) { buf[i] = 0; }
        for(int i=0 ; i<_poly ; i++)
        {   
            _voices[i].render(_audio_tmp.data(), len);
            for(int i=0 ; i<len ; i++) { buf[i] += _audio_tmp[i]; }
        }
    }
};

} // wade