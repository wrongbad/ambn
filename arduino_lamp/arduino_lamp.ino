#include <pitchToFrequency.h>
#include <pitchToNote.h>
#include <frequencyToNote.h>
#include <MIDIUSB.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Arduion Pro Micro A3 = 21
#define PIN 21

#define LEDCOUNT 165

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LEDCOUNT, PIN, NEO_GRB + NEO_KHZ800);

uint8_t rgbs[3*LEDCOUNT];

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

void setup() {
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  pinMode(21, OUTPUT);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  
  for(uint16_t i=0 ; i<3*LEDCOUNT ; i++)
  {
    rgbs[i] = 8;
  }

  Serial.begin(115200);
}

uint32_t rng_x = 12345;
uint32_t rng()
{
    rng_x ^= rng_x << 13;
    rng_x ^= rng_x >> 17;
    rng_x ^= rng_x << 5;
    return rng_x;
}

//  uint8_t maxs[3] = {248,248,100};
int mins[3] = {4, 4, 4};
int maxs[3] = {20,20,10};
//int dx = 8;

int ctl[8] = {0};
bool ctl_active = false;

int lightning = 0;

void loop()
{
    rng_x ^= analogRead(20); // hardware entropy

    if(ctl_active)
    {
        maxs[0] = maxs[1] = maxs[2] = 20;
        for(int i=0 ; i<6 ; i++)
        {
            maxs[i/2] += ctl[i] * 2;
            for(int j=0 ; j<3 ; j++) { maxs[j] -= ctl[i]/2; }
        }
        for(int i=0 ; i<3 ; i++) { mins[i] = maxs[i] / 4; }
    }
  
    // walk
    for(uint16_t i=0 ; i<3*LEDCOUNT ; i++)
    {
        bool dir = rng() & 1;
        int dx = rgbs[i] / 12;
        if(dx < 2) dx = 2;
        if(dir && rgbs[i]-dx >= mins[i%3] && rgbs[i]-dx >= 0)
            rgbs[i] -= dx;
        else if(!dir && rgbs[i]+dx <= maxs[i%3] && rgbs[i]+dx <= 255)
            rgbs[i] += dx;
    }

    // flow for 12.5 circumference
    for(uint16_t i=0 ; i<3*(LEDCOUNT-13) ; i+=3)
    {
        bool swap = (rng() & 127) == 0;
        if(!swap) { continue; }
        
        int which = 3 * (12 + (rng() & 1));
        
        for(int j=0 ; j<3 ; j++)
        {
            uint8_t tmp = rgbs[i+j];
            rgbs[i+j] = rgbs[i+j + which];
        //    rgbs[i + which] = tmp;
        }
    }

    if(lightning > 0)
    {
        
        for(uint16_t i=0 ; i<3*LEDCOUNT ; i++)
        {
            rgbs[i] += (256 - rgbs[i]) / 3; 
        }
    
        lightning = 0;
    }

    // refresh
    for(uint16_t i=0 ; i<LEDCOUNT ; i++)
    {
        strip.setPixelColor(i, strip.Color(rgbs[i*3], rgbs[i*3+1], rgbs[i*3+2]));
    }
    strip.show();

    // midi recv
    while(1)
    {
        midiEventPacket_t pkt = MidiUSB.read();
        // no more messages
        if(pkt.header == 0) { break; }
        // only ctl messages
        if(pkt.header != 0xB) { continue; }
        
        if(pkt.byte2 < 8)
        { 
            ctl[pkt.byte2] = pkt.byte3;
            ctl_active = true;
        }
        if(pkt.byte2 == 10)
        {
            lightning = 16;
            ctl_active = true;
        }
    }
    
    delay(20);
}
